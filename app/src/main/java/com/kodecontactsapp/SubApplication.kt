package com.kodecontactsapp

import android.app.Application
import com.kodecontactsapp.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class SubApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@SubApplication)
            modules(listOf(appModule))
        }
    }
}