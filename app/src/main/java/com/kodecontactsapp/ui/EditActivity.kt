package com.kodecontactsapp.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kodecontactsapp.R
import com.kodecontactsapp.databinding.ActivityEditBinding
import com.kodecontactsapp.ui.dialog.NoteDialogFragment
import com.kodecontactsapp.ui.dialog.NoteDialogListener
import com.kodecontactsapp.ui.dialog.RingtoneDialogFragment
import com.kodecontactsapp.ui.dialog.RingtoneDialogListener
import com.kodecontactsapp.ui.viewmodel.ProcessContactViewModel
import com.kodecontactsapp.util.ContactImageUtils
import com.kodecontactsapp.util.Converter
import com.kodecontactsapp.vo.Contact
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.bottom_sheet_dialog.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.io.IOException

class EditActivity : AppCompatActivity(), RingtoneDialogListener,
    NoteDialogListener {

    companion object {
        private const val PERMISSION_REQUEST_CODE = 0
        private const val REQUEST_IMAGE_GALLERY = 1
        private const val REQUEST_IMAGE_CAMERA = 2
        const val INTENT_CONTACT = "CONTACT"
        fun start(context: Context?, contact: Contact?) = context?.startActivity(
            Intent(context, EditActivity::class.java).apply {
                putExtra(INTENT_CONTACT, contact)
            }
        )
    }

    private lateinit var bottomSheet: BottomSheetDialog
    private val processContactViewModel: ProcessContactViewModel by viewModel()
    private val contactImageUtils: ContactImageUtils by inject()
    private val converter: Converter by inject()
    private var contact: Contact? = null
    private var pictureUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityEditBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_edit
        )
        binding.apply {
            lifecycleOwner = this@EditActivity
            viewModel = processContactViewModel
        }
        setSupportActionBar(findViewById(R.id.toolbarEdit))
        bottomSheet = BottomSheetDialog(this).apply {
            setContentView(R.layout.bottom_sheet_dialog)
        }

        contact = intent.getParcelableExtra<Contact?>(INTENT_CONTACT)
        processContactViewModel.defineContactState(contact)
        setListeners()
        contactImageUtils.loadPicture(contact?.pictureUri, ivPicture, R.drawable.placeholder_contact_picture)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_items_edit, menu)
        supportActionBar?.title = getString(R.string.appbar_title_edit)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.action_done -> {
                processContactViewModel.processContact(contact?.id, pictureUri)
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    bottomSheet.show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_GALLERY)
                pictureUri = data?.data
            contactImageUtils.loadPicture(pictureUri, ivPicture, R.drawable.placeholder_contact_picture)
            bottomSheet.cancel()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun showRingtoneDialog() {
        val dialog = RingtoneDialogFragment()
        dialog.show(supportFragmentManager, "RingtoneDialogFragment")
    }

    override fun onRingtoneDialogPositive(dialog: DialogFragment, index: Int) {
        tietRingtone.setText(resources.getStringArray(R.array.ringtones)[index])
    }

    private fun showNoteDialog() {
        val dialog = NoteDialogFragment(tietNote.text.toString())
        dialog.show(supportFragmentManager, "NoteDialogFragment")
    }

    override fun onNoteDialogPositiveClick(dialog: DialogFragment, note: String) {
        tietNote.setText(note)
    }

    private fun setListeners() {
        btnDeleteContact.setOnClickListener {
            contact?.let {
                processContactViewModel.deleteContact(it)
            }
            finish()
        }
        ivPicture.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED
            ) {
                bottomSheet.show()
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    PERMISSION_REQUEST_CODE
                )
            }
        }
        bottomSheet.apply {
            tvCamera.setOnClickListener { takePictureViaCamera() }
            tvGallery.setOnClickListener { pickPictureFromGallery() }
        }
        tietRingtone.setOnClickListener { showRingtoneDialog() }
        tietNote.setOnClickListener { showNoteDialog() }
    }

    private fun pickPictureFromGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "image/*"
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_GALLERY)
        }
    }

    private fun takePictureViaCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                val photoFile: File? = try {
                    contactImageUtils.createImageFile()
                } catch (ex: IOException) {
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.kodecontactsapp.fileprovider",
                        it
                    )
                    pictureUri = photoURI
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(
                        takePictureIntent,
                        REQUEST_IMAGE_CAMERA
                    )
                }
            }
        }
    }
}
