package com.kodecontactsapp.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.kodecontactsapp.R
import com.kodecontactsapp.databinding.ActivityMainBinding
import com.kodecontactsapp.ui.viewmodel.ContactViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val contactViewModel: ContactViewModel by viewModel()
    private val rvAdapter = ContactsNamesListAdapter { EditActivity.start(this@MainActivity, it) }
    private lateinit var searchView: SearchView
    private var query = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        binding.apply {
            lifecycleOwner = this@MainActivity
            viewModel = contactViewModel
        }
        setSupportActionBar(findViewById(R.id.toolbarMain))

        rvContacts.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = rvAdapter
        }
        contactViewModel.searchContact("%")
    }

    override fun onResume() {
        super.onResume()
        if (::searchView.isInitialized) searchView.setQuery(query, true)
        contactViewModel.searchContact("%$query%")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_items_main, menu)
        supportActionBar?.title = getString(R.string.appbar_title_contacts)
        searchView = (menu?.findItem(R.id.action_search)?.actionView as SearchView).apply {
            setOnQueryTextListener(onQueryTextListener())
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
        when (item?.itemId) {
            R.id.action_add_contact -> {
                EditActivity.start(this@MainActivity, null)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun onQueryTextListener() =
        object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    query = it
                    contactViewModel.searchContact("%$it%")
                }
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }
        }
}
