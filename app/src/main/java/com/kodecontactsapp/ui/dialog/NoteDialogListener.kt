package com.kodecontactsapp.ui.dialog

import androidx.fragment.app.DialogFragment

interface NoteDialogListener {
    fun onNoteDialogPositiveClick(dialog: DialogFragment, note: String)
}