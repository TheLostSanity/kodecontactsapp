package com.kodecontactsapp.ui.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.kodecontactsapp.R
import kotlinx.android.synthetic.main.dialog_note.view.*

class NoteDialogFragment(private val note: String?) : DialogFragment() {

    private lateinit var listener: NoteDialogListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as NoteDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement NoticeDialogListener")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val view = requireActivity().layoutInflater.inflate(R.layout.dialog_note, null)
            view.tietNoteDialog.setText(note)
            builder.setView(view)
                .setTitle(getString(R.string.dialog_note_title))
                .setPositiveButton(R.string.dialog_note_positive) { _, _ ->
                    listener.onNoteDialogPositiveClick(this, view.tietNoteDialog.text.toString())
                }
                .setNegativeButton(getString(R.string.dialog_button_cancel)) { _, _ -> }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}