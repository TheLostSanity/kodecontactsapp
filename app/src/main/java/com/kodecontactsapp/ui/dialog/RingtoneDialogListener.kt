package com.kodecontactsapp.ui.dialog

import androidx.fragment.app.DialogFragment

interface RingtoneDialogListener {
    fun onRingtoneDialogPositive(dialog: DialogFragment, index: Int)
}