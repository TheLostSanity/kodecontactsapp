package com.kodecontactsapp.ui.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.kodecontactsapp.R
import kotlinx.android.synthetic.main.item_dialog_ringtone.view.*

class RingtoneDialogFragment : DialogFragment() {

    private lateinit var listener: RingtoneDialogListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as RingtoneDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement NoticeDialogListener")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val view = requireActivity().layoutInflater.inflate(R.layout.dialog_ringtone, null)
            var index = 0
            builder.setView(view)
                .setTitle(getString(R.string.dialog_ringtones_title))
                .setPositiveButton(R.string.dialog_ringtones_positive) { _, _ ->
                    listener.onRingtoneDialogPositive(this, index)
                }
                .setNegativeButton(getString(R.string.dialog_button_cancel)) { _, _ -> }
            view.findViewById<ListView>(R.id.lvRingtones).apply {
                divider = resources.getDrawable(android.R.color.transparent, null)
                val arrayAdapter = ArrayAdapter<String>(
                    it,
                    R.layout.item_dialog_ringtone,
                    R.id.textView,
                    resources.getStringArray(R.array.ringtones)
                )
                adapter = arrayAdapter
                setOnItemClickListener { _, v, position, _ ->
                    (v as LinearLayout).textView.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.baseline_done_24,
                        0
                    )
                    (getChildAt(index) as LinearLayout).textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                    index = position
                }
            }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}
