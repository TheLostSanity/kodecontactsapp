package com.kodecontactsapp.ui.binding

interface BindableAdapter<T> {
    fun setData(data: T?)
}