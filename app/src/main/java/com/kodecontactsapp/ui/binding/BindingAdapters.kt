package com.kodecontactsapp.ui.binding

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.kodecontactsapp.vo.ContactState

@BindingAdapter("data")
fun <T> setRecyclerViewData(recyclerView: RecyclerView, data: T?) {
    if (recyclerView.adapter is BindableAdapter<*>) {
        (recyclerView.adapter as BindableAdapter<T>).setData(data)
    }
}

@BindingAdapter("uiState")
fun setVisibilityByContactState(view: View, state: ContactState) {
    when (state) {
        ContactState.ADDING -> view.visibility = View.GONE
        ContactState.EDITING -> view.visibility = View.VISIBLE
    }
}