package com.kodecontactsapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.kodecontactsapp.R
import com.kodecontactsapp.ui.binding.BindableAdapter
import com.kodecontactsapp.vo.Contact

class ContactsNamesListAdapter(private val onContactClick: (Contact) -> Unit) :
    RecyclerView.Adapter<ContactsNamesListAdapter.ContactsViewHolder>(),
    BindableAdapter<List<Contact>> {

    private var contacts = emptyList<Contact>()

    class ContactsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val item: ConstraintLayout = itemView.findViewById(R.id.constraintLayout)
        val fullName: TextView = itemView.findViewById(R.id.tvName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_rv_contacts, parent, false)
        return ContactsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ContactsViewHolder, pos: Int) {
        val curr = contacts[pos]
        holder.item.setOnClickListener { onContactClick(curr) }
        holder.fullName.text = curr.getFullName()
    }

    override fun getItemCount(): Int = contacts.size

    override fun setData(data: List<Contact>?) {
        data?.let {
            contacts = it
            notifyDataSetChanged()
        }
    }
}