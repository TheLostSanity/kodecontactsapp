package com.kodecontactsapp.ui.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kodecontactsapp.repository.ContactRepository
import com.kodecontactsapp.vo.Contact
import com.kodecontactsapp.vo.ContactState
import kotlinx.coroutines.launch

class ProcessContactViewModel(private val repository: ContactRepository) : ViewModel() {

    private val _contact = MutableLiveData(Contact())
    val contact: LiveData<Contact> = _contact
    private val _contactState = MutableLiveData<ContactState>()
    val contactState: LiveData<ContactState> = _contactState

    fun defineContactState(contact: Contact?) {
        when (contact) {
            null -> {
                _contactState.value = ContactState.ADDING
            }
            else -> {
                _contactState.value = ContactState.EDITING
                this._contact.value = contact
            }
        }
    }

    fun processContact(id: Int?, pictureUri: Uri?) {
        _contact.value?.let { contact ->
            id?.let { contact.id = it }
            contact.pictureUri = pictureUri
        }
        contact.value?.let {
            viewModelScope.launch {
                when (contactState.value) {
                    ContactState.ADDING -> {
                        repository.insert(it)
                    }
                    ContactState.EDITING -> {
                        repository.update(it)
                    }
                }
            }
        }
    }

    fun deleteContact(contact: Contact) {
        viewModelScope.launch {
            repository.delete(contact)
        }
    }
}