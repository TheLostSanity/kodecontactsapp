package com.kodecontactsapp.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kodecontactsapp.repository.ContactRepository
import com.kodecontactsapp.vo.Contact
import kotlinx.coroutines.launch

class ContactViewModel(private val repository: ContactRepository) : ViewModel() {

    private val _allContacts = MutableLiveData<List<Contact>>().apply {
        observeForever { repository.allContacts }
    }
    val allContacts: LiveData<List<Contact>> = _allContacts

    fun searchContact(query: String) {
        viewModelScope.launch {
            _allContacts.value = repository.search(query)
        }
    }
}