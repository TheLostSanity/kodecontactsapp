package com.kodecontactsapp.di

import androidx.room.Room
import com.kodecontactsapp.db.ContactDatabase
import com.kodecontactsapp.repository.ContactRepository
import com.kodecontactsapp.ui.viewmodel.ContactViewModel
import com.kodecontactsapp.ui.viewmodel.ProcessContactViewModel
import com.kodecontactsapp.util.ContactImageUtils
import com.kodecontactsapp.util.Converter
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { Room.databaseBuilder(get(), ContactDatabase::class.java, "contact_database").build() }
    single { get<ContactDatabase>().contactDao() }
    single { Converter() }
    single { ContactRepository(get(), get()) }
    single { ContactImageUtils(androidContext()) }
    viewModel { ContactViewModel(get()) }
    viewModel { ProcessContactViewModel(get()) }
}