package com.kodecontactsapp.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.kodecontactsapp.db.ContactDao
import com.kodecontactsapp.util.Converter
import com.kodecontactsapp.vo.Contact

class ContactRepository(private val contactDao: ContactDao, private val converter: Converter) {

    val allContacts: LiveData<List<Contact>> =
        Transformations.map(contactDao.getContacts()) { list ->
            list.map { converter.contactEntityToContact(it) }
        }

    suspend fun search(query: String): List<Contact> {
        return contactDao.searchContact(query).map { converter.contactEntityToContact(it) }
    }

    suspend fun insert(contact: Contact): Long {
        return converter.contactToContactEntity(contact)?.let { contactDao.insertContact(it) } ?: -2
    }

    suspend fun update(contact: Contact) {
        converter.contactToContactEntity(contact)?.let {
            contactDao.updateContact(it)
        }
    }

    suspend fun delete(contact: Contact) {
        converter.contactToContactEntity(contact)?.let {
            contactDao.deleteContact(it)
        }
    }
}