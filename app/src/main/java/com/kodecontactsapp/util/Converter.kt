package com.kodecontactsapp.util

import com.kodecontactsapp.db.ContactEntity
import com.kodecontactsapp.vo.Contact
import com.kodecontactsapp.vo.FirstLastNames

class Converter {

    fun contactToContactEntity(contact: Contact?): ContactEntity? {
        return contact?.let {
            ContactEntity(
                id = contact.id,
                firstLastNames = FirstLastNames(contact.firstName, contact.lastName),
                phone = contact.phone,
                ringtone = contact.ringtone,
                note = contact.note,
                pictureUri = contact.pictureUri
            )
        }
    }

    fun contactEntityToContact(contactEntity: ContactEntity): Contact {
        return Contact(
            id = contactEntity.id,
            firstName = contactEntity.firstLastNames?.firstName,
            lastName = contactEntity.firstLastNames?.lastName,
            phone = contactEntity.phone,
            ringtone = contactEntity.ringtone,
            note = contactEntity.note,
            pictureUri = contactEntity.pictureUri
        )
    }
}