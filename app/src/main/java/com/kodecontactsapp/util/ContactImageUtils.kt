package com.kodecontactsapp.util

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.widget.ImageView
import com.bumptech.glide.Glide
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ContactImageUtils(private val context: Context?) {

    @Throws(IOException::class)
    fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val storageDir: File? = context?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        )
    }

    fun loadPicture(uri: Uri?, target: ImageView, fallback: Int) {
        context?.let {
            uri?.let {
                Glide.with(context)
                    .load(it)
                    .circleCrop()
                    .fallback(fallback)
                    .into(target)
            }
        }
    }
}