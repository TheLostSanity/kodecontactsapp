package com.kodecontactsapp.vo

import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Contact(
    var id: Int? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var phone: String? = null,
    var ringtone: String? = null,
    var note: String? = null,
    var pictureUri: Uri? = null
) : Parcelable {

    fun getFullName() = when {
        firstName.isNullOrBlank() -> lastName
        lastName.isNullOrBlank() -> firstName
        else -> "$firstName $lastName"
    }
}
