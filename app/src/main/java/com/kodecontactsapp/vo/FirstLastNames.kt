package com.kodecontactsapp.vo

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FirstLastNames(
    var firstName: String?,
    var lastName: String?
) : Parcelable
