package com.kodecontactsapp.vo

enum class ContactState {
    ADDING,
    EDITING
}