package com.kodecontactsapp.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [ContactEntity::class], version = 1)
@TypeConverters(DatabaseConverters::class)
abstract class ContactDatabase : RoomDatabase() {

    abstract fun contactDao(): ContactDao
}