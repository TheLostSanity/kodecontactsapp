package com.kodecontactsapp.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ContactDao {

    @Query("SELECT * from ContactEntity ORDER BY firstName ASC")
    fun getContacts(): LiveData<List<ContactEntity>>

    @Query("SELECT * from ContactEntity WHERE firstName LIKE :query OR lastName LIKE :query ORDER BY firstName ASC")
    suspend fun searchContact(query: String): List<ContactEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertContact(contactEntity: ContactEntity): Long

    @Update
    suspend fun updateContact(contactEntity: ContactEntity)

    @Delete
    suspend fun deleteContact(contactEntity: ContactEntity)
}