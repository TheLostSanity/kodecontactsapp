package com.kodecontactsapp.db

import android.net.Uri
import androidx.room.TypeConverter

class DatabaseConverters {
    @TypeConverter
    fun uriToString(uri: Uri?): String? {
        return uri?.toString()
    }

    @TypeConverter
    fun stringToUri(string: String?): Uri? {
        return string?.let { Uri.parse(it) }
    }
}