package com.kodecontactsapp.db

import android.net.Uri
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.kodecontactsapp.vo.FirstLastNames

@Entity
data class ContactEntity(
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
    @Embedded val firstLastNames: FirstLastNames? = null,
    val phone: String? = null,
    val ringtone: String? = null,
    val note: String? = null,
    val pictureUri: Uri? = null
)
